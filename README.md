#polyglot rpn calculator

since there was no contrain to use client/server and/or deamons listener, i kept the architecture as simple as possible.

I chosen a piped process driven aproach that does the task 'quick and dirty'.

##USAGE
just run 


	./rpn-cli.rb


it will ask for how many expression rows (e.g: 2)
and then at each prompt '->' enter the rpn expression to solve.


	number of rows: 2
	-> 3 4 +
	-> 5 1 2 + 4 * + 3 -
	7  ,  14.519581ms
	14  ,  23.746529ms
	done.


