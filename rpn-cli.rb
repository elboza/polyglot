#!/usr/bin/ruby

require 'json'
require 'shellwords'

#DEBUG= true;
DEBUG= false;

class IsOp
	def self.===item
		return ['+','-','*','/'].include?(item);
	end
end

class IsNum
	def self.===item
		return item.numeric?
	end
end

class String
  def numeric?
    Float(self) != nil rescue false
  end
end

def to_numeric(str)
  Integer(str)
rescue
  Float(str) if Float(str) rescue nil
end

def parse(line)
	puts line if(DEBUG);
	line=line.split(' ');
	line.map{
		|x| case x
		when IsOp
			puts "is op '#{x}'" if(DEBUG);
			x
		when IsNum
			puts "is num '#{x}'" if(DEBUG);
			to_numeric(x)
		else
			puts "unknown '#{x}'"
			exit
		end
	}
end

def main
	arr=[];
	print "number of rows: "
	n=gets.chomp
	n.to_i.times{
		print "-> "
		line=gets.chomp;
		arr<<parse(line);
	}
	puts arr.to_s if(DEBUG);
	str="";
	a="go run rpn-mid.go ";
	arr.each{|x|
		str += JSON.generate(x) + " ";
	}
	cmd= a + Shellwords.escape(str);
	cmd.nil? ? nil : cmd.chomp(" ")
	puts cmd if(DEBUG);
	resp=%x[#{cmd}];
	puts resp;
	puts "done.\n"
end

main();