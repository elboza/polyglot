#!/usr/bin/python

import numbers
import sys
import json

#DEBUG=True
DEBUG=False

def is_op(x):
	return x in ['+','-','*','/']

def is_num(x):
	return isinstance(x,numbers.Number)

def calc(x):
	if DEBUG: print x
	params=[]
	while x:
		a=x.pop(0)
		if DEBUG: print params ,a ,x
		#if DEBUG: print params
		if is_op(a):
			#if DEBUG: print "is op {0}".format(a)
			b=reduce(lambda x,y:eval(str(x)+a+str(y)),params[-2:])
			if DEBUG: print b
			x.insert(0,b)
			params=params[:-2]
		elif is_num(a):
			#if DEBUG: print "is num {0}".format(a)
			params.append(a)
		else:
			print "unknown {0}".format(a)
			exit

	return params[0]



print calc(json.loads(sys.argv[1]))

