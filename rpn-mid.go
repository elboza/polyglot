package main
import (
	"fmt"
	"os"
	"strings"
	"os/exec"
	"time"
	"log"
)

func do(v string){
	//fmt.Print(v," ")
	if(v=="" || v == " ") {return;}
	f, err := os.OpenFile("rpn.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
    	panic(err)
	}
	defer f.Close()

	log.SetOutput(f)

	cmdName := "./rpn-worker.py"
	start := time.Now()
	cmd := exec.Command(cmdName, v)
 	out, err := cmd.Output()
 	elapsed := time.Since(start)
 	if err != nil {
 		fmt.Println(err.Error())
 		return
 	}
 	out2:=strings.TrimSpace(string(out))
 	fmt.Println(out2," , ",elapsed)
 	log.Println(v,out2,"==>",elapsed)
}

func main(){
    str:=strings.Split(os.Args[1]," ")
    //fmt.Println(str)
    
    for _,v:=range str{
    	do(strings.TrimSpace(v))
    }
}